from os import scandir
import csv

csvName=input("Enter csv filename:")
cards = open(csvName,"a")
writer = csv.writer(cards)

mediaDir=input("Name of media directory:")
media=scandir(mediaDir)

for i in media:
    question = "[sound:" + i.name + "]"
    answer = i.name.rstrip(".wav")

    row=[question,answer]
    writer.writerow(row)

    
