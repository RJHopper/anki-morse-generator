from pydub import AudioSegment
import csv

dash = AudioSegment.from_wav("dash.wav")
dot = AudioSegment.from_wav("dot.wav")
letterspace = AudioSegment.from_wav("intra-char-space.wav")


inputmethod = input("from csv or manual?: ")
if inputmethod=='csv':
    filename=input("type in file name: ")
    auto = True
    f=open(filename,'r')
    reader=csv.reader(f)
    
elif inputmethod == 'manual':
    auto=False
else:
    print('defaulting to manual')
    auto=False
    
while True:
    if auto:
        meaning, sequence = next(reader)
    else:
        meaning = input("input meaning: ")
        sequence = input("input code: ")
    
    
    dotdash=[]
    for i in sequence:
        if i=="-":
            dotdash.append(dash)
        elif i ==".":
            dotdash.append(dot)
        dotdash.append(letterspace)
 
    combined_sounds = sum(dotdash)
    combined_sounds.export("leters/" + meaning+".wav", format="wav")

f.close()
