import numpy
from scipy.io.wavfile import write

volume = 0.1
sample_rate = 44100
duration = 0.30
frequency = 0

samples = (numpy.sin(2 * numpy.pi * numpy.arange(sample_rate * duration)
     * frequency / sample_rate)).astype(numpy.float32)

write('letterspace.wav', sample_rate, samples)

