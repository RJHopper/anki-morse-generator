import pyaudio
import numpy as np

p = pyaudio.PyAudio()

volume = 0.1
fs = 44100
duration = 0.75

f = 440

samples = (np.sin(2 * np.pi * np.arange(fs * duration) * f / 
fs)).astype(np.float32).tobytes()

stream = p.open(format = pyaudio.paFloat32,
                channels = 1,
                rate = fs,
                output = True)

stream.write(samples)


