from pydub import AudioSegment
import csv

A = AudioSegment.from_wav("leters/A.wav")
B = AudioSegment.from_wav("leters/B.wav")
C = AudioSegment.from_wav("leters/C.wav")
D = AudioSegment.from_wav("leters/D.wav")
E = AudioSegment.from_wav("leters/E.wav")
F = AudioSegment.from_wav("leters/F.wav")
G = AudioSegment.from_wav("leters/G.wav")
H = AudioSegment.from_wav("leters/H.wav")
I = AudioSegment.from_wav("leters/I.wav")
J = AudioSegment.from_wav("leters/J.wav")
K = AudioSegment.from_wav("leters/K.wav")
L = AudioSegment.from_wav("leters/L.wav")
M = AudioSegment.from_wav("leters/M.wav")
N = AudioSegment.from_wav("leters/N.wav")
O = AudioSegment.from_wav("leters/O.wav")
P = AudioSegment.from_wav("leters/P.wav")
Q = AudioSegment.from_wav("leters/Q.wav")
R = AudioSegment.from_wav("leters/R.wav")
S = AudioSegment.from_wav("leters/S.wav")
T = AudioSegment.from_wav("leters/T.wav")
U = AudioSegment.from_wav("leters/U.wav")
V = AudioSegment.from_wav("leters/V.wav")
W = AudioSegment.from_wav("leters/W.wav")
X = AudioSegment.from_wav("leters/X.wav")
Y = AudioSegment.from_wav("leters/Y.wav")
Z = AudioSegment.from_wav("leters/Z.wav")

charspace = AudioSegment.from_wav("inter-char-space.wav")

letters={"A":A,"B":B,"C":C,"D":D,"E":E,"F":F,"G":G,"H":H,"I":I,"J":J,"K":K,"L":L,"M":M,"N":N,"O":O,"P":P,"Q":Q,"R":R,"S":S,"T":T,"U":U,"V":V,"W":W,"X":X,"Y":Y,"Z":Z}


inputmethod = input("from csv or manual?: ")
if inputmethod=='csv':
    filename=input("type in file name: ")
    auto = True
    f=open(filename,'r')
    reader=csv.reader(f)
elif inputmethod == 'manual':
    auto=False
else:
    print('defaulting to manual')
    auto=False


while True:
    if auto:
        row = next(reader)
        plainWord=row[0]
    else:
        plainWord = input("input word: ")

    morseWord=[]

    for i in plainWord.upper():
        morseWord.append(letters[i])
        morseWord.append(charspace)

    combined_sounds = sum(morseWord)
    combined_sounds.export("words/" + plainWord + ".wav", format="wav")
    
f.close()
