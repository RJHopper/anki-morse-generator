# morse-generator

A friend and I decided to learn Morse code. I wanted to use Anki.

So i spent the first hour writing this while my friend was learning morse.

# Credits and resources
Generating the inital dot and dash: https://stackoverflow.com/questions/52477889/how-to-write-pyaudio-output-into-audio-file

Combining the files to make letters:
https://stackoverflow.com/questions/2890703/how-to-join-two-wav-files-using-python

## Packages used that i had to install (pip install <name>):
- http://pydub.com/
- https://www.scipy.org/


# How to use bring into Anki
- import the .csv file into anki
- copy the generated sounds into the media folder
	- linux path: `.local/share/Anki2/ **username**/collection.media/`

	*might be slightly different, i don't have a mac and this is what i found online*
	- macOS path: `Library/Application Support/Anki2\ **username**`
	- windows path: `AppData\Roaming\Anki2\ **username**\collection.media`
- refresh/sync anki so the media syncs up
